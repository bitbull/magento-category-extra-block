Magento Category Extra Block
=====================
Adds in category page(in the left column) a block with the following data:
- image
- image label
- title
- subtitle
- link url
- link text

The block and be personalized from backend Manage Categories.
Can be enabled directly or time dependant(from -> to).

Can inherit data from parent categories(same usaga as Is Anchor). 

eg.

* if the current category is enabled and has data(at least image & link) then the block is displayed.
* if the current category is disabled then it search for parent data. If the parent is enabled and has data then the data is displayed. If not data is searched recursively until it finds a parent with data. If no parent is found then no data will be displayed.


Can be used also with flat categories activated.