<?php
$this->startSetup();


$this->addAttribute(
    Mage_Catalog_Model_Category::ENTITY, Bitbull_CategoryExtraBlock_Helper_Data::ADV_LINK_CATEG_ATTRIBUTE_CODE, array(
        'group'        => 'Adv',
        'type'         => 'varchar',
        'backend'      => '',
        'input'        => 'text',
        'label'        => 'Adv Link',
        'global'       => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'visible'      => true,
        'required'     => false,
        'user_defined' => true,
        'note'         => '',
        'sort_order' => 90
    )
);

$this->addAttribute(
    Mage_Catalog_Model_Category::ENTITY, Bitbull_CategoryExtraBlock_Helper_Data::ADV_LINK_TEXT_CATEG_ATTRIBUTE_CODE,
    array(
        'group'        => 'Adv',
        'type'         => 'varchar',
        'backend'      => '',
        'input'        => 'text',
        'label'        => 'Adv Button Text',
        'global'       => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'visible'      => true,
        'required'     => false,
        'user_defined' => true,
        'note'         => '',
        'sort_order' => 100
    )
);

$this->addAttribute(
    Mage_Catalog_Model_Category::ENTITY, Bitbull_CategoryExtraBlock_Helper_Data::ADV_TITLE_CATEG_ATTRIBUTE_CODE, array(
        'group'        => 'Adv',
        'type'         => 'varchar',
        'backend'      => '',
        'input'        => 'text',
        'label'        => 'Adv Title',
        'global'       => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'visible'      => true,
        'required'     => false,
        'user_defined' => true,
        'note'         => '',
        'sort_order' => 50
    )
);

$this->addAttribute(
    Mage_Catalog_Model_Category::ENTITY, Bitbull_CategoryExtraBlock_Helper_Data::ADV_SUBTITLE_CATEG_ATTRIBUTE_CODE,
    array(
        'group'        => 'Adv',
        'type'         => 'varchar',
        'backend'      => '',
        'input'        => 'text',
        'label'        => 'Adv Subtitle',
        'global'       => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'visible'      => true,
        'required'     => false,
        'user_defined' => true,
        'note'         => '',
        'sort_order' => 60
    )
);

$this->addAttribute(
    Mage_Catalog_Model_Category::ENTITY, Bitbull_CategoryExtraBlock_Helper_Data::ADV_IMAGE_LABEL_CATEG_ATTRIBUTE_CODE,
    array(
        'group'        => 'Adv',
        'type'         => 'varchar',
        'backend'      => '',
        'input'        => 'text',
        'label'        => 'Adv image label',
        'global'       => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'visible'      => true,
        'required'     => false,
        'user_defined' => true,
        'note'         => '',
        'sort_order' => 80
    )
);

$this->addAttribute(
    Mage_Catalog_Model_Category::ENTITY, Bitbull_CategoryExtraBlock_Helper_Data::ADV_IMAGE_CATEG_ATTRIBUTE_CODE, array(
        'group'        => 'Adv',
        'type'         => 'varchar',
        'backend'      => 'catalog/category_attribute_backend_image',
        'input'        => 'image',
        'label'        => 'Adv image',
        'global'       => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'visible'      => true,
        'required'     => false,
        'user_defined' => true,
        'note'         => '',
        'sort_order' => 70
    )
);

$this->addAttribute(
    Mage_Catalog_Model_Category::ENTITY, Bitbull_CategoryExtraBlock_Helper_Data::ADV_CHILD_EXTEND_CATEG_ATTRIBUTE_CODE,
    array(
        'group'        => 'Adv',
        'type'         => 'int',
        'source'       => 'eav/entity_attribute_source_boolean',
        'input'        => 'select',
        'backend'      => '',
        'frontend'     => '',
        'label'        => 'Adv is used in child categories',
        'global'       => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'visible'      => true,
        'required'     => false,
        'user_defined' => true,
        'note'         => '',
        'sort_order' => 40
    )
);

$this->addAttribute(
    Mage_Catalog_Model_Category::ENTITY, Bitbull_CategoryExtraBlock_Helper_Data::ADV_START_DATE_CATEG_ATTRIBUTE_CODE, array(
        'group'        => 'Adv',
        'type'         => 'datetime',
        'source'       => 'eav/entity_attribute_backend_datetime',
        'input'        => 'date',
        'backend'      => '',
        'frontend'     => '',
        'label'        => 'Adv start date',
        'global'       => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'visible'      => true,
        'required'     => false,
        'user_defined' => true,
        'note'         => 'optional',
        'sort_order' => 20
    )
);

$this->addAttribute(
    Mage_Catalog_Model_Category::ENTITY, Bitbull_CategoryExtraBlock_Helper_Data::ADV_END_DATE_CATEG_ATTRIBUTE_CODE, array(
        'group'        => 'Adv',
        'type'         => 'datetime',
        'source'       => 'eav/entity_attribute_backend_datetime',
        'input'        => 'date',
        'backend'      => '',
        'frontend'     => '',
        'label'        => 'Adv end date',
        'global'       => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'visible'      => true,
        'required'     => false,
        'user_defined' => true,
        'note'         => 'optional',
        'sort_order' => 30
    )
);

$this->addAttribute(
    Mage_Catalog_Model_Category::ENTITY, Bitbull_CategoryExtraBlock_Helper_Data::ADV_IS_ENABLE_CATEG_ATTRIBUTE_CODE, array(
        'group'        => 'Adv',
        'type'         => 'int',
        'source'       => 'eav/entity_attribute_source_boolean',
        'input'        => 'select',
        'backend'      => '',
        'frontend'     => '',
        'label'        => 'Enabled',
        'global'       => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'visible'      => true,
        'required'     => false,
        'user_defined' => true,
        'note'         => '',
        'sort_order' => 10
    )
);



$this->endSetup();