<?php
/* @var $installer Mage_Eav_Model_Entity_Setup */
$installer = $this;
$installer->startSetup();


$installer->removeAttribute(
    Mage_Catalog_Model_Category::ENTITY, Bitbull_CategoryExtraBlock_Helper_Data::ADV_START_DATE_CATEG_ATTRIBUTE_CODE
);

$installer->removeAttribute(
    Mage_Catalog_Model_Category::ENTITY, Bitbull_CategoryExtraBlock_Helper_Data::ADV_END_DATE_CATEG_ATTRIBUTE_CODE
);

$installer->addAttribute(
    Mage_Catalog_Model_Category::ENTITY, Bitbull_CategoryExtraBlock_Helper_Data::ADV_START_DATE_CATEG_ATTRIBUTE_CODE, array(
        'group'        => 'Adv',
        'type'         => 'datetime',
        'input'        => 'date',
        'backend'      => '',
        'frontend'     => '',
        'label'        => 'Adv start date',
        'global'       => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'visible'      => true,
        'required'     => false,
        'user_defined' => true,
        'note'         => 'optional',
        'sort_order' => 20
    )
);

$this->addAttribute(
    Mage_Catalog_Model_Category::ENTITY, Bitbull_CategoryExtraBlock_Helper_Data::ADV_END_DATE_CATEG_ATTRIBUTE_CODE, array(
        'group'        => 'Adv',
        'type'         => 'datetime',
        'input'        => 'date',
        'backend'      => '',
        'frontend'     => '',
        'label'        => 'Adv end date',
        'global'       => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'visible'      => true,
        'required'     => false,
        'user_defined' => true,
        'note'         => 'optional',
        'sort_order' => 30
    )
);


$installer->endSetup();