<?php
/**
 * @category Bitbull
 * @package  Bitbull_CategoryExtraBlock
 * @author   Dan Nistor <dan.nistor@bitbull.it>
 */

class Bitbull_CategoryExtraBlock_Block_Category_Adv extends Mage_Core_Block_Template
{

    /**
     * Current category
     *
     * @var Mage_Catalog_Model_Category
     */
    protected $_category;

    /**
     * Parent ids from witch can inherit block data
     */
    protected $_inheritParent;

    /**
     * Adv data
     */
    protected $_blockData;

    protected $_advIsEnabled;
    protected $_advIsTimeActive;


    /**
     * Get current category
     *
     * @return Mage_Catalog_Model_Category
     */
    protected function _getCurrentCategory()
    {
        if (!isset($this->_category)) {
            $this->_category = Mage::registry("current_category");
        }
        return $this->_category;
    }


    /**
     * Get category data based on $attribute code
     *
     * @param $attributeCode
     *
     * @return string
     */
    protected function _getCategoryAdvData($attributeCode)
    {
        return $this->_getCurrentCategory()->getData($attributeCode);
    }


    /**
     * Get parent ids from witch can inherit block data
     *
     * @return array|null
     */
    protected function _inheritParent()
    {
        if (isset($this->_inheritParent)) {
            return $this->_inheritParent;
        } else {
            $parentIds = $this->_getCurrentCategory()->getParentIds();
            $inheritAttributeCode = Bitbull_CategoryExtraBlock_Helper_Data::ADV_CHILD_EXTEND_CATEG_ATTRIBUTE_CODE;

            /** @var Bitbull_CategoryExtraBlock_Helper_Data $helper */
            $helper = Mage::helper("bitbull_categoryextrablock");
            $this->_inheritParent = $helper->getAdvParentId($parentIds, $inheritAttributeCode);
        }
        return $this->_inheritParent;
    }


    /**
     * Get adv block data
     *  if current category has adv data load it
     *  otherwise search if can load load parent data
     *  if yes then load it, otherwise null
     *
     * @return array
     */

    public function getAdvData()
    {
        if (isset($this->_blockData)) {
            return $this->_blockData;
        }

        //check if can load current category adv data
        //se la categoria e' enabled, attiva nel Tempo, ha i dati viene caricata
        //se no si passa alla prossima categoria
        if ($this->advIsActive() && $this->_advHasData()) {
            $this->_blockData = array_merge(
                $this->_blockData,
                array(
                    "title"       => $this->_getCategoryAdvData(
                            Bitbull_CategoryExtraBlock_Helper_Data::ADV_TITLE_CATEG_ATTRIBUTE_CODE
                        ),
                    "subtitle"    => $this->_getCategoryAdvData(
                            Bitbull_CategoryExtraBlock_Helper_Data::ADV_SUBTITLE_CATEG_ATTRIBUTE_CODE
                        ),
                    "image_label" => $this->_getCategoryAdvData(
                            Bitbull_CategoryExtraBlock_Helper_Data::ADV_IMAGE_LABEL_CATEG_ATTRIBUTE_CODE
                        ),
                )
            );

            return $this->_blockData;
        } else if (count($this->_inheritParent()) > 0) {
            //load data form parent category

            //have to do same loop check as for current category
            $nearestParentCategoryId = array_pop($this->_inheritParent);

            $category = Mage::getModel("catalog/category")->load($nearestParentCategoryId);
            $this->_category = $category;
            $this->_advIsEnabled = null;
            $this->_advIsTimeActive = null;


            return $this->getAdvData();
        }

        return $this->_blockData;
    }


    /**
     * Check if category adv data time is active in current day
     *
     * @return bool
     */
    protected function _advTimeActive()
    {
        if (!isset($this->_advIsTimeActive)) {

            $advStart = null;
            $advEnd = null;

            if ($this->_getCategoryAdvData(
                Bitbull_CategoryExtraBlock_Helper_Data::ADV_START_DATE_CATEG_ATTRIBUTE_CODE
            )
            ) {
                $advStart = strtotime(
                    $this->_getCategoryAdvData(
                        Bitbull_CategoryExtraBlock_Helper_Data::ADV_START_DATE_CATEG_ATTRIBUTE_CODE
                    )
                );
                $advStart = date('Y-m-d', $advStart);
            }
            if ($this->_getCategoryAdvData(Bitbull_CategoryExtraBlock_Helper_Data::ADV_END_DATE_CATEG_ATTRIBUTE_CODE)) {
                $advEnd = strtotime(
                    $this->_getCategoryAdvData(
                        Bitbull_CategoryExtraBlock_Helper_Data::ADV_END_DATE_CATEG_ATTRIBUTE_CODE
                    )
                );
                $advEnd = date('Y-m-d', $advEnd);
            }
            $currentTimestamp = Mage::getModel('core/date')->timestamp(time());
            $currentDate = date('Y-m-d', $currentTimestamp);

            //if adv date interval contains current day then adv is active
            //if start and end are null then adv is active
            if (($advStart <= $currentDate || $advStart == null)
                && ($advEnd >= $currentDate || $advEnd == null)
            ) {
                $this->_advIsTimeActive = true;
            } else {
                $this->_advIsTimeActive = false;
            }
        }
        return $this->_advIsTimeActive;
    }


    /**
     * Check if current category adv data can be used
     *
     * @return bool
     */
    public function advIsActive()
    {
        if ($this->_advIsEnabled() && $this->_advTimeActive()) {
            return true;
        } else {
            return false;
        }
    }


    /**
     * Check if category has adv data that can be used
     *
     * @return bool
     */
    protected function _advHasData()
    {
        //if has img and link
        $image = $this->_getCategoryAdvData(Bitbull_CategoryExtraBlock_Helper_Data::ADV_IMAGE_CATEG_ATTRIBUTE_CODE);
        $link = $this->_getCategoryAdvData(Bitbull_CategoryExtraBlock_Helper_Data::ADV_LINK_CATEG_ATTRIBUTE_CODE);
        $textLink = $this->_getCategoryAdvData(
            Bitbull_CategoryExtraBlock_Helper_Data::ADV_LINK_TEXT_CATEG_ATTRIBUTE_CODE
        );

        if (!is_null($image)) {
            if (is_null($textLink)) {
                $textLink = Bitbull_CategoryExtraBlock_Helper_Data::DEFAULT_ADV_BUTTON_TEXT;
            }

            $this->_blockData = array(
                "image_url" => $image,
                "link_url"  => $link,
                "link_text" => $textLink
            );
            return true;
        }
        return false;
    }


    /**
     * Check if category adv is enabled
     *
     * @return boolean
     */
    protected function _advIsEnabled()
    {
        if (!isset($this->_advIsEnabled)) {
            $this->_advIsEnabled = $this->_getCategoryAdvData(
                Bitbull_CategoryExtraBlock_Helper_Data::ADV_IS_ENABLE_CATEG_ATTRIBUTE_CODE
            );
        }
        return $this->_advIsEnabled;
    }


    /**
     * Resize image
     *
     * @param $fileName
     * @param $width
     * @param $height
     *
     * @return string image url
     */
    public function resizeImage($fileName, $width, $height = '')
    {
        return Mage::helper("bitbull_categoryextrablock")->resizeImage($fileName, $width, $height);
    }


}