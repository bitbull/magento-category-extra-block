<?php

/**
 * @category Bitbull
 * @package  Bitbull_CategoryExtraBlock
 * @author   Dan Nistor <dan.nistor@bitbull.it>
 */
class Bitbull_CategoryExtraBlock_Helper_Data extends Mage_Core_Helper_Abstract
{

    const ADV_TITLE_CATEG_ATTRIBUTE_CODE = "adv_title";
    const ADV_SUBTITLE_CATEG_ATTRIBUTE_CODE = "adv_subtitle";
    const ADV_LINK_CATEG_ATTRIBUTE_CODE = "adv_link";
    const ADV_LINK_TEXT_CATEG_ATTRIBUTE_CODE = "adv_link_text";
    const ADV_IMAGE_LABEL_CATEG_ATTRIBUTE_CODE = "adv_image_label";
    const ADV_IMAGE_CATEG_ATTRIBUTE_CODE = "adv_image";
    const ADV_CHILD_EXTEND_CATEG_ATTRIBUTE_CODE = "child_extend";
    const ADV_START_DATE_CATEG_ATTRIBUTE_CODE = "adv_start_date";
    const ADV_END_DATE_CATEG_ATTRIBUTE_CODE = "adv_end_date";
    const ADV_IS_ENABLE_CATEG_ATTRIBUTE_CODE = "adv_is_enable";

    /**
     * Default block link button text
     */
    const DEFAULT_ADV_BUTTON_TEXT = "Discover More";


    /**
     * Get all parent categories ids from which current adv can inherit data
     *
     * @param array $parentIds
     * @param string $attributeCode
     *
     * @return array|null
     */
    public function getAdvParentId(array $parentIds, $attributeCode)
    {
        $catalogCategoryResource = Mage::getModel("catalog/category")->getResource();
        $inheritCategoriesIds = null;

        //use for NON FLAT CATEGORIES
        if (!$this->_isCatalogFlat()) {
            $attribute = Mage::getSingleton('catalog/config')
                ->getAttribute(Mage_Catalog_Model_Category::ENTITY, $attributeCode);

            $inheritCategoriesIds = $catalogCategoryResource
                ->findWhereAttributeIs($parentIds, $attribute, 1);
        } else {
            $select = $catalogCategoryResource->getReadConnection()
                ->select()
                ->from(array('e' => $catalogCategoryResource->getMainStoreTable($this->_getStoreId())), 'entity_id')
                ->where($attributeCode . ' = ?', 1)
                ->where('entity_id IN (?)', $parentIds);

            $inheritCategoriesIds = $catalogCategoryResource
                ->getReadConnection()
                ->fetchCol($select);
        }
        return $inheritCategoriesIds;
    }


    /**
     * Check if magento catalog has Flat Categories Enabled
     *
     * @return boolean
     */
    protected function _isCatalogFlat()
    {
        return Mage::helper("catalog/category_flat")->isEnabled();
    }


    /**
     * Get store id
     *
     * @return int
     */
    protected function _getStoreId()
    {
        return Mage::app()->getStore()->getId();
    }


    /**
     * @param        $fileName
     * @param        $width
     * @param string $height
     *
     * @return string image url
     */
    public function resizeImage($fileName, $width, $height = '')
    {
        $folderURL = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . 'catalog/category';
        $imageURL = $folderURL . DS . $fileName;

        $basePath = Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . 'catalog/category' . DS . $fileName;
        $newPath = Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . 'catalog/category' . DS . 'resized' . DS
            . $fileName;
        //if width empty then return original size image's URL
        if ($width != '') {
            //if image has already resized then just return URL
            if (file_exists($basePath) && is_file($basePath) && !file_exists($newPath)) {
                $imageObj = new Varien_Image($basePath);
                $imageObj->constrainOnly(true);
                $imageObj->keepAspectRatio(true);
                $imageObj->keepFrame(false);
                $imageObj->resize($width, $height);
                $imageObj->save($newPath);
            }
            $resizedURL =
                Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . 'catalog/category' . DS . "resized" . DS
                . $fileName;
        } else {
            $resizedURL = $imageURL;
        }
        return $resizedURL;
    }


}